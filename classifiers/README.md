Получение предиктов классификаторов сказка/твиттер/новость/Лев Толстой:

```angular2html
clf = MulticlassClassifier()
predictions = clf.predict(list_of_sentences, ['fairytale', 'twitter', 'news', 'tolstoy']) #returns np.array
```
